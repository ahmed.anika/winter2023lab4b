public class Dogs {
	
	private String speed;
	private String diet;
	private int age;

	public Dogs(String speed, String diet, int age) {
		this.speed = speed;
		this.diet = diet;
		this.age = age;
	}

	public void EatFoods() {
		if ( this.age < 9 ) {
			this.diet = "Strict";
		} else {
			this.diet = "Anything";
		}
		System.out.println(this.diet);
	}
	
	public void RunFast() {
		if (this.age < 7) {
			this.speed = "Slow";
		} else {
			this.speed = "Fast";
		}
		System.out.println(this.speed);
	}
	
	//set methods
	
	public void setAge(int age){
		this.age = age;
	}
	
	//get methods
	public String getSpeed(){
		return this.speed;
	}
	public String getDiet(){
		return this.diet;
	}
	public int getAge(){
		return this.age;
	}
}