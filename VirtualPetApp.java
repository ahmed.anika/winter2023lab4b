import java.util.Scanner;

public class VirtualPetApp {
	public static void main (String [] args) {
		Dogs[] pack = new Dogs[1];
		for (int i = 0; i < pack.length; i++) {
			java.util.Scanner reader = new java.util.Scanner(System.in);
			//input dog speed
			System.out.println("How fast is the dog? Fast or Slow?");
			String speed = reader.nextLine();
			//input dog diet
			System.out.println("What diet does the dog have? Strict or Anything?");
			String diet = reader.nextLine();
			//input dog age
			System.out.println("How old is the dog?");
			int age = Integer.parseInt(reader.nextLine());
			pack[i] = new Dogs(speed, diet, age);
		}	
		pack[0].setAge(14);
		System.out.println(pack[0].getSpeed());
		System.out.println(pack[0].getDiet());
		System.out.println(pack[0].getAge());
	}
}